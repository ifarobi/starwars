var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swig = require('swig');
var request = require('request');
var app = express();

var url = {
  "people" : "http://swapi.co/api/people",
  "films" : "http://swapi.co/api/films",
  "starships" : "http://swapi.co/api/starships",
  "vehicles" : "http://swapi.co/api/vehicles",
  "species" : "http://swapi.co/api/species",
  "planets" : "http://swapi.co/api/planets",
};

app.engine('html', swig.renderFile);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(favicon(path.join(__dirname, 'public/favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));

app.set('view cache', false);
swig.setDefaults({cache : false});

app.get('/', function(req,res){
  res.render('defaults');
});

app.get('/:id(people|films|starships|vehicles|species|planets)', function(req,res){
  var dataType = req.params.id;
  console.log(url[dataType]);
  request.get(url[dataType], function(err, resp, body){
    if(!err && resp.statusCode == 200) {
      var data = JSON.parse(body);
      console.log(data);
      res.render('index', {state : dataType, data : data});
    }
  });
});

app.listen(3000, function() {
  console.log('app running on port 3000');
});
