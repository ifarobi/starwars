// jshint loopfunc : true


// Observer pattern
function Event(sender){
  this._sender = sender;
  this._listeners = [];
}

Event.prototype = {
  attach : function(listener){
    this._listeners.push(listener);
  },
  notify : function (args) {
    var i;
    for(i = 0; i < this._listeners.length; i++){
      this._listeners[i](this._sender, args);
    }
  }
};

function IFR(urls, state, menus, elements){
  this._urls = urls;
  this._state = state;
  this._menus = menus;
  this._model = new Model();
  this._view = new View();
  this._controller = new Controller();
  this._elements = elements;
}

IFR.prototype = {
  updateContent : function(url, callback){
    console.log("whyyyyy");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if(xhttp.readyState == 4){
        if(xhttp.status == 200){
          // updateContent
          callback(xhttp.response);
        }
      }
    };
    xhttp.open("GET",url, true);
    xhttp.send();
  },
  build : function() {
    this._view.setModel(this._model);
    this._view.setElements(this._elements);
    this._view.setMenu(this._menus);
    this._controller.setUrls(this._urls);
    this._controller.setModel(this._model);
    this._controller.setView(this._view);
  },
  run : function(){
    var _this = this;
    window.history.replaceState(
      this._urls[this.state],
      null,
      document.location.href
    );
    window.addEventListener('popstate', function(e){
      if(e.state != null){
        _this.updateContent(e.state, function(result){
          _this._model.setData(result);
        });
      }
    });
    this.build();
  },
  clickMenuHandler : function(urls,event) {

  },
};

function Model(){
  this._data = null;
  this._nextUrl = null;
  this._items = [];
  this.itemAdded = new Event(this);
  this.selected = new Event(this);

}

Model.prototype = {
  setData : function(data){
    this._data = data;
    this._nextUrl = this._data.next;
    this._items = this._data.results;
    console.log("Model::constructor");
  },
  getItems : function(){
    return [].concat(this._items);
  },
  loadPage : function(){

  },
  loadMoreItem : function(next, callback){
    console.log("Model::loadMoreItem");
    var xhttp = new XMLHttpRequest();
    var _this = this;
    xhttp.onreadystatechange = function(){
      if(xhttp.readyState == 4){
        if(xhttp.status == 200){
          data = JSON.parse(xhttp.response);
          _this._items.push(data.results);
          state = next.split('/')[4]
          _this._nextUrl = data.next;
          callback(data, state);
        }
      }
    };
    xhttp.open("GET", next, true);
    xhttp.send();
  }
};



function View(){
  this._model = null;
  this._elements = null;
  this._menus = null;

  this.loadMoreClicked = new Event(this);
  this.menuClicked = new Event(this);


}

View.prototype = {
  setMenu : function(menus){
    this._menus = menus;
    var _this = this;
    for(i = 0; i < this._menus.length; i++){
      menus[i].addEventListener('click', function(event){
        event.preventDefault();
        var state = event.target.getAttribute('href').split('/').pop();
        _this.menuClicked.notify({"state" : state});
      }, true);
    }
  },
  setModel : function(model){
    this._model = model;
  },
  setElements : function(elements){
    this._elements = elements;
    var _this = this;
    this._elements.loadMore.addEventListener("click", function(e){
      _this.loadMoreClicked.notify();
    });
  },
  generateItem : function(data, state){
    var name, item, html='';
    var d;
      console.log(data);
    for(d in data){
      item = "";
      name = data[d].name;
      if(state == "films") name = data[d].title;
      switch(state){
        case "people" :
          item += '<span title="Films"><i class="icon-small films"></i>'+data[d].films.length+'</span>';
          item += '<span title="Starships"><i class="icon-small starships"></i>'+data[d].starships.length+'</span>';
          item += '<span title="Vehicles"><i class="icon-small vehicles"></i>'+data[d].vehicles.length+'</span>';
          break;
        case "planets" :
          item += '<span title="Films"><i class="icon-small films"></i>'+data[d].films.length+'</span>';
          break;
        case "starships" :
          item += '<span title="Pilots"><i class="icon-small people"></i>'+data[d].pilots.length+'</span>';
          item += '<span title="Films"><i class="icon-small films"></i>'+data[d].films.length+'</span>';
          break;
        case "vehicles" :
          item += '<span title="Pilots"><i class="icon-small people"></i>'+data[d].pilots.length+'</span>';
          item += '<span title="Films"><i class="icon-small films"></i>'+data[d].films.length+'</span>';
          break;
        case "species" :
          item += '<span title="Pilots"><i class="icon-small people"></i>'+data[d].people.length+'</span>';
          item += '<span title="Films"><i class="icon-small films"></i>'+data[d].films.length+'</span>';
          break;
        case "films" :
          item += '<span title="Pilots"><i class="icon-small people"></i>'+data[d].characters.length+'</span>';
          item += '<span title="Planets"><i class="icon-small planets"></i>'+data[d].planets.length+'</span>';
          item += '<span title="Starships"><i class="icon-small starships"></i>'+data[d].starships.length+'</span>';
          item += '<span title="Vehicles"><i class="icon-small vehicles"></i>'+data[d].vehicles.length+'</span>';
          break;
      }
      html += '<div class="item-box" type="'+state+'">';
      html += '<div class="identity"><div class="data-type left">';
      html += '<i class="icon '+state+'"></i></div>';
      html += '<h1 class="left">'+name+'</h1>';
      html += '<div class="clear"></div></div><div class="slight-info">'+item+'</div>';
      html += '<div class="feature-btn">';
      html += '<span class="love" title="love"><i class="icon-small love"></i>13</span>';
      html += '<span class="comments" title="comments"><i class="icon-small comments"></i>15</span>';
      html += '<a class="detail right" href="#detail">Detail</a><div class="clear"></div></div></div>';
    }
    this._elements.content.innerHTML = this._elements.content.innerHTML+html

  },
  show : function() {
    this.buildLists();
  },
  buildLists : function() {
    var items = this._model.getItems();
    console.log("View::buildLists");
  },
  updatePage : function(url, callback){
    this._elements.content.innerHTML = '';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if(xhttp.readyState == 4){
        if(xhttp.status == 200){
          // updateContent
          callback(xhttp.response);
        }
      }
    };
    xhttp.open("GET",url, true);
    xhttp.send();
  }
};

function Controller() {
  this._model = null;
  this._view = null;
  this._urls = null;

  var _this = this;


}

Controller.prototype = {
  setModel : function(model) {
    this._model = model;
  },
  setView : function(view) {
    this._view = view;
    var _this = this;
    this._view.loadMoreClicked.attach(function() {
      _this.loadMoreItem();
    });
    this._view.menuClicked.attach(function(sender, args) {
      _this.menuHandler(args.state);
    });
  },
  setUrls : function(urls) {
    this._urls = urls;
  },
  menuHandler : function(state) {
    var _this = this;
    window.history.pushState(
      this._urls[state],
      null,
      event.target.href
    );
    //view
    this._view.updatePage(this._urls[state], function(result){
      //cont
      console.log("Controller:menuHandler");
      var r = JSON.parse(result);
      _this._model.setData(r);
      if(r.next == null){
        _this._view._elements.loadMore.innerHTML = "No more "+state+".";
      }else{
        _this._view._elements.loadMore.innerHTML = "Load More";
        _this._view._elements.loadMore.setAttribute('next',r.next);
      }
      _this._view.generateItem(r.results, state);
    });

  },
  loadMoreItem : function() {
    console.log("Controller::loadMoreItem");
    var _this = this;
    this._model.loadMoreItem(this._view._elements.loadMore.getAttribute('next'),function(data, state){
      if(data.next == null){
        _this._view._elements.loadMore.innerHTML = "No more "+state+".";
      }else{
        _this._view._elements.loadMore.setAttribute('next',data.next);
        _this._view._elements.loadMore.innerHTML = "Load More";
      }
      _this._view.generateItem(data.results, state);
    });
  }
};
