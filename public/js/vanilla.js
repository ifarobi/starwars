// jshint loopfunc : true
// Observer pattern
function Event(sender){
  this._sender = sender;
  this._listeners = [];
}

Event.prototype = {
  attach : function(listener){
    this._listeners.push(listener);
  },
  notify : function (args) {
    var i;
    for(i = 0; i < this._listeners.length; i++){
      this._listeners[i](this._sender, args);
    }
  }
};


function ListModel(data){
  data = JSON.parse(data);
  this._nextUrl = data.next;
  this._items = data.results;
  this.itemAdded = new Event(this);
  this.selected = new Event(this);

}

ListModel.prototype = {
  getItems : function(){
    return [].concat(this._items);
  },
  loadMoreItem : function(callback){
    var xhttp = new XMLHttpRequest();
    var _this = this;
    xhttp.onreadystatechange = function(){
      if(xhttp.readyState == 4){
        if(xhttp.status == 200){
          data = JSON.parse(xhttp.response);
          _this._nextUrl = data.next;
          _this._items.push(data.results);
          callback(data.results);
        }
      }
    };
    xhttp.open("GET", this._nextUrl, true);
    xhttp.send();
  }
};



function View(model, elements){
  this._model = model;
  this._elements = elements;

  this.loadMoreClicked = new Event(this);

  var _this = this;

  this._elements.loadMore.addEventListener("click", function(e){
    _this.loadMoreClicked.notify();
  });
}

View.prototype = {
  show : function() {
    this.buildLists();
  },
  buildLists : function() {
    var items = this._model.getItems();
  }
};

function Controller(model, view) {
  this._model = model;
  this._view = view;
  var _this = this;
  this._view.loadMoreClicked.attach(function() {
    _this.loadMoreItem();
  });
}

Controller.prototype = {
  loadMoreItem : function() {
    console.log("Controller::loadMoreItem");
    this._model.loadMoreItem(function(results){
      console.log(results);
    });
  }
};
